function Color(red, green, blue, alpha){
	if(isNaN(red) || isNaN(green) || isNaN(blue)){
		throw "Invalid input for color object";
	}
	this.red = (red<255)?(red<0)?0:red:255;
	this.green = (green<255)?(green<0)?0:green:255;
	this.blue = (blue<255)?(blue<0)?0:blue:255;
	if(alpha && !isNaN(alpha))
		this.alpha = (alpha<1)?(alpha<0)?0:alpha:1;
}

function getHexCode(number){
	var hex = number.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function getHexColorCode(colorObj){
	return '#'+getHexCode(colorObj.red)+getHexCode(colorObj.green)+getHexCode(colorObj.blue)
}

function getRGBAString(colorObj){
	return 'rgba('+colorObj.red+', '+colorObj.green+', '+colorObj.blue+', '+colorObj.alpha+')'
}

module.exports = {
			Color: Color,
			getHexCode: getHexCode,
			getHexColorCode: getHexColorCode,
			getRGBAString: getRGBAString
		};