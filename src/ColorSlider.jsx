var React = require('react');
var ColorLib = require('./Color');
var Color = ColorLib.Color;

var ColorSlider = React.createClass({
	isDraggable: false,

	getDefaultProps: function() {
		return {
			startRGB: new Color(255,255,255),
			endRGB: new Color(0,0,0),
			sliderWidth: 100,
			onRender: null,
			title: '',
			lengthVal: 255,
			initVal:0
		};
	},

	propTypes: {
		startRGB : React.PropTypes.shape({
      		red: React.PropTypes.number.isRequired,
      		green: React.PropTypes.number.isRequired,
			blue: React.PropTypes.number.isRequired
    	}),
    	endRGB : React.PropTypes.shape({
      		red: React.PropTypes.number.isRequired,
      		green: React.PropTypes.number.isRequired,
			blue: React.PropTypes.number.isRequired
    	}),
    	sliderWidth : React.PropTypes.number,
    	onRender: React.PropTypes.func,
    	title: React.PropTypes.string
	},

	getInitialState: function() {
		return {
			 sliderPosition:this.props.initVal
		};
	},

	getSliderPosition: function(){
		return ((this.props.sliderWidth-1) * this.state.sliderPosition) / this.props.lengthVal;
	},

	getGradientCSS: function(rgb1, rgb2){
		var HCL = ColorLib.getHexColorCode(rgb1);
		var HCR = ColorLib.getHexColorCode(rgb2);

		return {
			'backgroundImage' : 'linear-gradient(right, '+HCL+', '+HCR+')',
			'backgroundImage' : '-o-linear-gradient(right, '+HCL+', '+HCR+')',
			'backgroundImage' : '-ms-linear-gradient(right, '+HCL+', '+HCR+')',
			'backgroundImage' : '-moz-linear-gradient(right, '+HCL+', '+HCR+')',
			'backgroundImage' : '-webkit-linear-gradient(right, '+HCL+', '+HCR+')',
		};
	},

	onClick: function(e){
		var postionInPixels = e.clientX - e.target.parentElement.offsetLeft + 1;
		var postionInColorScale = Math.floor(((postionInPixels * (this.props.lengthVal))/this.props.sliderWidth));
		console.log(postionInPixels+":"+postionInColorScale)
		this.setState({
			sliderPosition: postionInColorScale
		});
		if(this.props.onRender){
			this.props.onRender();
		}
	},

	onMouseDown: function(e){
		this.isDraggable = true
	},
	
	onMouseUp: function(e){
		this.isDraggable = false
	},
	
	onMouseMove: function(e){
		if(this.isDraggable){
			this.onClick(e)
		}
	},

	render: function(){
		var positionCSS = {
			left: this.getSliderPosition()+'px'
		};

		var sliderWidth = {
			width:this.props.sliderWidth+'px'
		};

		var events = {
			onClick: this.onClick,
			onMouseUp: this.onMouseUp,
			onMouseDown: this.onMouseDown,
			onMouseOut: this.onMouseUp,
			onMouseMove: this.onMouseMove
		}

		return (
			<div className="slider-control-group" style={sliderWidth}>
				{this.props.title?<div>{this.props.title+" - "+this.state.sliderPosition}</div>:''}
				<span className="slider-ticker" style={positionCSS}>|</span>
				<div className="slider" style={this.getGradientCSS(this.props.startRGB, this.props.endRGB)} {...events}></div>
			</div>
		);
	}
});

module.exports = ColorSlider;
