var React = require('react');
var ReactDOM = require('react-dom');

var SimpleColorPicker = React.createClass({
	getInitialState: function(){
		return {
			red: this.props.red,
			green: this.props.green,
			blue: this.props.blue,
			alpha: this.props.alpha,
			colorCSS: this.getColorCSS({
				red: this.props.red,
				green: this.props.green,
				blue: this.props.blue,
				alpha: this.props.alpha
			})
		}
	},

	getDefaultProps: function(){
		return {
			red: 0,
			green: 0,
			blue: 0,
			alpha: 1
		}
	},

	getColorCSS: function(obj){
		return {
			background: 'rgba('+obj.red+','+obj.green+','+obj.blue+','+obj.alpha+')'
		}
	},

	handleChange: function(){
		this.setState({
			red: this.refs.red.value,
			green: this.refs.green.value,
			blue: this.refs.blue.value,
			alpha: this.refs.alpha.value,
			colorCSS: this.getColorCSS(this.state)
		})
	},

	render: function(){
		return (
			<div>
				<label htmlFor="red">Red: {this.state.red}</label>
				<input id="red" ref="red" type="range" min="0" max="255" value={this.state.red} step="1" onChange={this.handleChange}/>
				<label htmlFor="green">Green: {this.state.green}</label>
				<input id="green" ref="green" type="range" min="0" max="255" value={this.state.green} step="1" onChange={this.handleChange}/>
				<label htmlFor="blue">Blue: {this.state.blue}</label>
				<input id="blue" ref="blue" type="range" min="0" max="255" value={this.state.blue} step="1" onChange={this.handleChange}/>
				<label htmlFor="alpha">Alpha: {this.state.alpha}</label>
				<input id="alpha" ref="alpha" type="range" min="0" max="1" value={this.state.alpha} step="0.1" onChange={this.handleChange}/>
				
				<div className="color-box" style={this.state.colorCSS}>
				</div>
			</div>
		);
	}
});

module.exports = SimpleColorPicker;