var React = require('react');
var ColorLib = require('./Color');
var ColorSlider = require('./ColorSlider');
var Color = ColorLib.Color;

var ColorPicker = React.createClass({
	getDefaultProps: function() {
		return {
			sliderWidth: 300,
			color: new Color(0,0,0,0)
		};
	},

	getInitialState: function() {
		return this.props.color
	},

	onRender: function(){
		this.setState({
			red:this.refs.red.state.sliderPosition,
			green:this.refs.green.state.sliderPosition,
			blue:this.refs.blue.state.sliderPosition,
			alpha:this.refs.alpha.state.sliderPosition/100
		});
	},

	render: function(){
		var CSS = {background: ColorLib.getRGBAString(this.state)}

		return (
				<div>
					<ColorSlider ref="red" title="Red" initVal={this.state.red} onRender={this.onRender} sliderWidth={this.props.sliderWidth} startRGB={new Color(255,0,0)} endRGB={new Color(0,0,0)}/>
					<ColorSlider ref="green" title="Green" initVal={this.state.green} onRender={this.onRender} sliderWidth={this.props.sliderWidth} startRGB={new Color(0,255,0)} endRGB={new Color(0,0,0)}/>
					<ColorSlider ref="blue" title="Blue" initVal={this.state.blue} onRender={this.onRender} sliderWidth={this.props.sliderWidth} startRGB={new Color(0,0,255)} endRGB={new Color(0,0,0)}/>
					<ColorSlider ref="alpha" title="Alpha" initVal={this.state.alpha*100} lengthVal={100} onRender={this.onRender} sliderWidth={this.props.sliderWidth} startRGB={new Color(0,0,0)} endRGB={new Color(255,255,255)}/>

					<div className="color-box" style={CSS}></div>
				</div>
			);
	}
});

module.exports = ColorPicker