var React = require('react');
var ReactDOM = require('react-dom');
var Color = require('./Color');
var ColorPicker = require('./ColorPicker');
var SimpleColorPicker = require('./SimpleColorPicker');

ReactDOM.render(
	(<div>
	<ColorPicker color={new Color.Color(34,123,212,0.7)}/>
	<hr/>
	<SimpleColorPicker red="233"/>
	</div>), document.getElementById('content'));